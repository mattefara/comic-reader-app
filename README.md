# Comic Reader

This app is an university project.

## Setup

This application is run using a local Firebase environment using emulators.
To use it run inside the `firebase` folder

```shell
./scripts/firebase-launch.sh
```

## Data collection

The data are scraped using this [Scrapy Comics](https://gitlab.com/mattefara/scrapy-comic)