package com.reader.comicreader.threading

import android.content.Context
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.reader.comicreader.utils.Config


class ImageDownloader private constructor(
    private val context: Context,
    private val url: String,
    private val view: ImageView
) {

    class Builder {

        private lateinit var context: Context
        private lateinit var url: String
        private lateinit var view: ImageView

        fun use(context: Context): Builder = this.apply { this.context = context }
        fun load(url: String): Builder = this.apply { this.url = url }
        fun into(view: ImageView): Builder = this.apply { this.view = view }

        fun build(): ImageDownloader {
            return ImageDownloader(context, url, view)
        }

    }

    fun download() {
        if (!Config.DOWNLOAD_IMAGE) {
            Log.d(Config.TAG, "Download functionality is disabled by config")
            return
        }
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(view)

    }
}