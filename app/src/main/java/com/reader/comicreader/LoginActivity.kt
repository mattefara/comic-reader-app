package com.reader.comicreader

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.reader.comicreader.utils.Config
import com.reader.comicreader.utils.database.UserDatabaseInterface

class LoginActivity : AppCompatActivity() {

    private lateinit var authStateListener: AuthStateListener
    private val signIn =
        registerForActivityResult(FirebaseAuthUIActivityResultContract()) { result ->
            this.onSignInResult(result)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        authStateListener = AuthStateListener() {
            if (it.currentUser != null) {
                goToMainActivity()
            } else {
                performLogin()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        UserDatabaseInterface.auth.addAuthStateListener(authStateListener)
    }

    override fun onPause() {
        super.onPause()
        UserDatabaseInterface.auth.removeAuthStateListener(authStateListener)
    }

    private fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
        if (result.resultCode == RESULT_OK) {
            Log.d(Config.TAG, "Sign in successful!")
            goToMainActivity()
        } else {
            val response = result.idpResponse
            val message = if (response == null) {
                "Sign in canceled".apply {
                    Log.w(Config.TAG, this)
                }
            } else {
                "Sign in error".apply {
                    Log.w(Config.TAG, this, response.error)
                }
            }
            Toast.makeText(
                this,
                message,
                Toast.LENGTH_LONG).show()
        }
    }

    private fun performLogin() {
        Log.d(Config.TAG, "Performing login")
        UserDatabaseInterface.performLogin {
            val signInIntent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(
                    listOf(
                        AuthUI.IdpConfig.EmailBuilder().build()
                    )
                )
                .setTheme(R.style.Theme_ComicReader)
                .build()
            signIn.launch(signInIntent)
        }
    }

    private fun goToMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}