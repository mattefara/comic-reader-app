package com.reader.comicreader.utils.list

import androidx.recyclerview.widget.SortedList

abstract class FilterableRecyclerView<I, VH : BindableRecycleView.BindableViewHolder<I>>(

) :
    BindableRecycleView<I, VH>(), GrowableList<I> {

    protected val items by lazy { onCreateSortedList() }

    abstract fun onCreateSortedList(): SortedList<I>

    override fun getItemCount(): Int {
        return items.size()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
    }
}