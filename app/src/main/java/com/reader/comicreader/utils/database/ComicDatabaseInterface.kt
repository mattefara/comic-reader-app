package com.reader.comicreader.utils.database

import com.google.firebase.firestore.ktx.toObject
import com.reader.comicreader.data.ComicCardDataClass
import com.reader.comicreader.data.document.ChapterDocument
import com.reader.comicreader.data.document.ComicDocument
import kotlinx.coroutines.tasks.await

object ComicDatabaseInterface {

    private const val COLLECTION = "comics"
    private val firestore
        get() = DatabaseInterface.firestore

    suspend fun getOne(id: String): ComicDocument {
        return firestore.collection(COLLECTION)
            .document(id)
            .get()
            .await()
            .toObject()!!
    }

    suspend fun search(
        title: String,
        limit: Long,
        postProcess: ((item: ComicCardDataClass) -> ComicCardDataClass)? = null
    ): List<ComicCardDataClass> {
        val searchResults = firestore.collection(COLLECTION)
            .whereGreaterThanOrEqualTo("titleLower", title)
            .whereLessThanOrEqualTo("titleLower", "$title\uf8ff")
            .orderBy("titleLower")
            .limit(limit)
            .get()
            .await()
        return searchResults.map { result ->

            val firstChapter = (result.get("chapters") as List<*>).last().run {
                val parsed = this as MutableMap<*, *>
                ChapterDocument(
                    chapterId = parsed["chapterId"] as String,
                    chapterLink = parsed["chapterLink"] as String,
                    chapterNumber = (parsed["chapterNumber"] as Long).toInt(),
                    chapterName = parsed["chapterName"] as String
                )
            }

            ComicCardDataClass(
                comicId = result.id,
                title = result.get("title") as String,
                cover = result.get("cover") as String,
                providerId = result.get("provider") as String,
                firstChapter = firstChapter,
                updatedAt = result.get("updatedAt") as Long
            ).apply {
                postProcess?.invoke(this)
            }
        }
    }
}