package com.reader.comicreader.utils.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BindableRecycleView<
        I,
        VH : BindableRecycleView.BindableViewHolder<I>,
        >(private val items: List<I> = listOf()) :
    RecyclerView.Adapter<VH>() {

    abstract class BindableViewHolder<I>(view: View) : RecyclerView.ViewHolder(view), Bindable<I>

    protected fun rootView(parent: ViewGroup, layout: Int): View {
        return LayoutInflater.from(parent.context).inflate(layout, parent, false)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}