package com.reader.comicreader.utils.list

interface GrowableList<I> {
    fun add(items: List<I>)
    fun remove(item: I)
    fun remove(items: List<I>)
    fun replaceAll(items: List<I>)
}