package com.reader.comicreader.utils.database

import com.google.firebase.firestore.ktx.toObject
import com.reader.comicreader.data.document.ProviderDocument
import kotlinx.coroutines.tasks.await

object ProviderDatabaseInterface {

    private const val COLLECTION = "provider"
    private val firestore
        get() = DatabaseInterface.firestore

    suspend fun getOne(id: String): ProviderDocument {
        return firestore.collection(COLLECTION)
            .document(id)
            .get()
            .await()
            .toObject()!!
    }
}