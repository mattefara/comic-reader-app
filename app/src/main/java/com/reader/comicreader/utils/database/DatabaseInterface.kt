package com.reader.comicreader.utils.database

import android.util.Log
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.reader.comicreader.BuildConfig
import com.reader.comicreader.utils.Config

object DatabaseInterface {

    private const val LOCAL_IP_ADDRESS = "10.0.2.2"
    private const val LOCAL_FIRESTORE_PORT = 8080
    private const val LOCAL_AUTH_PORT = 9099

    init {
        if (BuildConfig.DEBUG) {
            Log.d(Config.TAG, "Connected to development database")
            Firebase.firestore.useEmulator(LOCAL_IP_ADDRESS, LOCAL_FIRESTORE_PORT)
            Firebase.auth.useEmulator(LOCAL_IP_ADDRESS, LOCAL_AUTH_PORT)
        }
    }

    val firestore
        get() = Firebase.firestore

    val auth
        get() = Firebase.auth

}