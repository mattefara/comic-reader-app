package com.reader.comicreader.utils.database

import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import com.reader.comicreader.data.BookmarkAggregate
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.data.document.AttributeDocument
import com.reader.comicreader.data.document.BookmarkDocument
import com.reader.comicreader.data.document.ChapterDocument
import kotlinx.coroutines.tasks.await

object BookmarkDatabaseInterface {

    private val firestore
        get() = DatabaseInterface.firestore

    private const val COLLECTION = "bookmarks"

    suspend fun getUpdates(): List<BookmarkDocument> {
        return firestore.collection(COLLECTION)
            .whereEqualTo("userId", Firebase.auth.uid)
            .orderBy("updatedAt", Query.Direction.DESCENDING)
            .get()
            .await()
            .toObjects()
    }

    fun aggregate(bookmarks: List<BookmarkDocument>): List<BookmarkAggregate> {
        val updates = arrayListOf<BookmarkAggregate>()
        var previousProvider: String? = null
        for (bookmark in bookmarks) {
            if (updates.isEmpty() || bookmark.providerId != previousProvider) {
                updates.add(
                    BookmarkAggregate(
                        providerLogo = bookmark.attributes.providerLogo,
                        updatedAt = bookmark.updatedAt,
                        updates = arrayListOf()
                    )
                )
            }
            val updatesCount =
                bookmark.lastChapterPublished.chapterNumber - (bookmark.lastChapterRead?.chapterNumber
                    ?: 0)
            if (updatesCount < 0) {
                continue
            }
            val last = updates.last()
            last.updates.add(
                BookmarkCardDataClass(
                    comicId = bookmark.comicId,
                    title = bookmark.title,
                    cover = bookmark.attributes.cover,
                    updatesCount = updatesCount,
                    lastChapterRead = bookmark.lastChapterRead,
                    providerId = bookmark.providerId,
                    updatedAt = bookmark.updatedAt
                )
            )
            previousProvider = bookmark.providerId
        }
        return updates
    }

    suspend fun add(id: String, lastChapter: ChapterDocument? = null) {
        val bookmark = firestore.collection(COLLECTION)
            .document(id)
            .get()
            .await()

        if (bookmark.exists()) {
            return
        }

        val comic = ComicDatabaseInterface.getOne(id)
        val provider = ProviderDatabaseInterface.getOne(comic.provider)

        firestore.collection(COLLECTION)
            .document(id)
            .set(
                BookmarkDocument(
                    title = comic.title,
                    userId = Firebase.auth.uid!!,
                    comicId = id,
                    providerId = comic.provider,
                    attributes = AttributeDocument(
                        cover = comic.cover,
                        providerLogo = provider.providerImageUrl
                    ),
                    lastChapterPublished = comic.lastChapter,
                    lastChapterRead = lastChapter,
                    updatedAt = comic.updatedAt
                ), SetOptions.merge()
            )
            .await()
    }

    suspend fun remove(comicId: String) {
        firestore.collection(COLLECTION)
            .document(comicId)
            .delete()
            .await()
    }

    fun subscribe(timestamp: Long, listener: EventListener<QuerySnapshot>): ListenerRegistration {
        return firestore.collection(COLLECTION)
            .whereEqualTo("userId", Firebase.auth.uid)
            .whereGreaterThan("updatedAt", timestamp)
            .orderBy("updatedAt", Query.Direction.DESCENDING)
            .addSnapshotListener(listener)
    }

    fun updateLastRead(id: String, chapter: ChapterDocument, comicInfo: Map<String, Any>? = null) {
        val update = mapOf<String, Any>(
            "lastChapterRead" to chapter,
        ) + (comicInfo ?: mapOf())

        firestore.collection(COLLECTION)
            .document(id)
            .set(update, SetOptions.merge())
    }
}