package com.reader.comicreader.utils.database

typealias MissingAuthenticationCallback<T> = () -> T

object UserDatabaseInterface {

    val auth
        get() = DatabaseInterface.auth


    fun performLogin(onMissingAuthentication: MissingAuthenticationCallback<Unit>) {
        if (auth.currentUser == null) {
            onMissingAuthentication()
        }
    }

}