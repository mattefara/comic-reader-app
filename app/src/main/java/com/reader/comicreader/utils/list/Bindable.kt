package com.reader.comicreader.utils.list

interface Bindable<T> {
    fun bind(item: T)
}