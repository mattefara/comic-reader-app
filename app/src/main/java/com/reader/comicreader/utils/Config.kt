package com.reader.comicreader.utils

import android.util.Log

object Config {
    const val TAG = "Comics Reader"
    const val DOWNLOAD_IMAGE = true

    fun log(clazz: Class<out Any>, message: String) {
        Log.d(clazz.simpleName, message)
    }
}