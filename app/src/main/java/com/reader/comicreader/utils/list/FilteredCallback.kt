package com.reader.comicreader.utils.list

import androidx.recyclerview.widget.SortedList

class FilteredCallback<I, VH : BindableRecycleView.BindableViewHolder<I>>(
    private val adapter: FilterableRecyclerView<I, VH>,
    private val comparator: Comparator<I>
) : SortedList.Callback<I>() {

    override fun onInserted(position: Int, count: Int) {
        adapter.notifyItemRangeInserted(position, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        adapter.notifyItemRangeRemoved(position, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        adapter.notifyItemMoved(fromPosition, toPosition)
    }

    override fun onChanged(position: Int, count: Int) {
        adapter.notifyItemChanged(position, count)
    }

    override fun compare(o1: I?, o2: I?): Int {
        return comparator.compare(o1, o2)
    }

    override fun areContentsTheSame(oldItem: I?, newItem: I?): Boolean {
        return oldItem?.equals(newItem) ?: false
    }

    override fun areItemsTheSame(item1: I?, item2: I?): Boolean {
        return item1.hashCode() == item2.hashCode()
    }
}