package com.reader.comicreader.data

import com.reader.comicreader.data.document.ChapterDocument

data class ComicCardDataClass(
    override var comicId: String,
    override var providerId: String,
    override val title: String,
    override val cover: String,
    override val updatedAt: Long,
    val firstChapter: ChapterDocument,
) : ComicInterface()