package com.reader.comicreader.data.document

data class ComicDocument(
    val chapters: List<ChapterDocument> = listOf(),
    val cover: String = "",
    val lastChapter: ChapterDocument = ChapterDocument(),
    val provider: String = "",
    val title: String = "",
    val titleLower: String = "",
    val updatedAt: Long = -1
)