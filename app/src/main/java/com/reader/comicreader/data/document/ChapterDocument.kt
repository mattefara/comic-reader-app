package com.reader.comicreader.data.document

data class ChapterDocument(
    val chapterId: String = "",
    val chapterName: String = "",
    val chapterNumber: Int = 0,
    val chapterLink: String = ""
)