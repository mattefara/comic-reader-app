package com.reader.comicreader.data

sealed class ComicInterface {
    abstract val comicId: String
    abstract val providerId: String
    abstract val title: String
    abstract val cover: String
    abstract val updatedAt: Long
}