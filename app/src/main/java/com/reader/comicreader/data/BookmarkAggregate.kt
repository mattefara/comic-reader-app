package com.reader.comicreader.data

import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


data class BookmarkAggregate(
    val providerLogo: String,
    var updatedAt: Long,
    val updates: ArrayList<BookmarkCardDataClass>
) {
    val formattedDate
        get(): String = run {
            val timestamp = Timestamp(updatedAt)
            val date = Date(timestamp.time)
            val calendar = Calendar.getInstance()
            calendar.time = date
            val today = Calendar.getInstance()
            val yesterday = Calendar.getInstance()
            yesterday.add(Calendar.DATE, -1)
            val timeFormatter: DateFormat = SimpleDateFormat("hh:mma", Locale.getDefault())

            return if (calendar[Calendar.YEAR] == today[Calendar.YEAR] && calendar[Calendar.DAY_OF_YEAR] == today[Calendar.DAY_OF_YEAR]) {
                "Today"
            } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(
                    Calendar.DAY_OF_YEAR
                ) == yesterday.get(Calendar.DAY_OF_YEAR)
            ) {
                "Yesterday"
            } else {
                timeFormatter.format(date)
            }
        }
}
