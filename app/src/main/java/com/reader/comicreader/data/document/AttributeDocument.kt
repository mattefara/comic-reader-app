package com.reader.comicreader.data.document

data class AttributeDocument(
    val cover: String = "",
    val providerLogo: String = ""
)