package com.reader.comicreader.data.document

data class ProviderDocument(
    val alias: String = "",
    val providerImageUrl: String = "",
    val providerRegex: String = ""
)