package com.reader.comicreader.data.document

data class BookmarkDocument(
    val attributes: AttributeDocument = AttributeDocument(),
    val comicId: String = "",
    val lastChapterPublished: ChapterDocument = ChapterDocument(),
    var lastChapterRead: ChapterDocument? = null,
    val providerId: String = "",
    val title: String = "",
    val updatedAt: Long = -1,
    val userId: String = ""
)