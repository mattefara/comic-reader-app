package com.reader.comicreader.data

import com.reader.comicreader.data.document.ChapterDocument

data class BookmarkCardDataClass(
    override val comicId: String,
    override val providerId: String,
    override val title: String,
    override val cover: String,
    override val updatedAt: Long,
    var updatesCount: Int,
    var lastChapterRead: ChapterDocument? = null,
) : ComicInterface()
