package com.reader.comicreader

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.reader.comicreader.data.document.ComicDocument
import com.reader.comicreader.databinding.ActivityWebComicBinding
import com.reader.comicreader.ui.webcomic.ComicWebViewClient
import com.reader.comicreader.utils.database.BookmarkDatabaseInterface
import com.reader.comicreader.utils.database.ComicDatabaseInterface
import com.reader.comicreader.utils.database.ProviderDatabaseInterface
import kotlinx.coroutines.launch

class WebComicActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebComicBinding

    private lateinit var comic: ComicDocument
    private var lastReadId: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebComicBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val comicId = intent.extras?.getString("comicId")!!
        val isBookmark = intent.extras?.getBoolean("isBookmark")!!
        val providerId = intent.extras?.getString("providerId")!!
        lastReadId = intent.extras?.getString("lastRead")

        lifecycleScope.launch {
            comic = ComicDatabaseInterface.getOne(comicId)
            val provider = ProviderDatabaseInterface.getOne(providerId)
            val lastReadChapter = comic.chapters.find { chapter -> chapter.chapterId == lastReadId }

            val webViewClient = ComicWebViewClient(
                applicationContext,
                provider.providerRegex,
            ) { url ->
                if (!isBookmark) {
                    return@ComicWebViewClient
                }
                comic.chapters
                    .find { chapter -> chapter.chapterLink == url.toString() }
                    ?.let { chapter ->
                        lastReadId = chapter.chapterId
                        BookmarkDatabaseInterface.updateLastRead(comicId, chapter)
                    }
            }
            binding.webComicView.webViewClient = webViewClient
            binding.webComicView.settings.javaScriptEnabled = true

            binding.webComicView.loadUrl(lastReadChapter!!.chapterLink)
        }

        binding.webComicViewClose.setOnClickListener {
            val intent = Intent().putExtra("chapterId", lastReadId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && binding.webComicView.canGoBack()) {
            binding.webComicView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}
