package com.reader.comicreader.notifications

import android.app.job.JobParameters
import android.app.job.JobService
import com.reader.comicreader.utils.Config

class NotificationService : JobService() {

    private var thread: ComicUpdateThread? = null

    override fun onStartJob(params: JobParameters?): Boolean {
        Config.log(this::class.java, "Service started")
        thread = ComicUpdateThread(this)
        thread?.start()
        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        Config.log(this::class.java, "Notification service is stopping")
        thread?.stopListen()
        return true
    }

    override fun onDestroy() {
        Config.log(this::class.java, "Service destroyed")
        super.onDestroy()
    }
}