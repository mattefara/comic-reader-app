package com.reader.comicreader.notifications

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import com.reader.comicreader.LoginActivity
import com.reader.comicreader.R

class ComicNotification(private val context: Context, title: String, content: String) {
    companion object {
        const val CHANNEL_NAME = "Realtime comics update"
        const val CHANNEL_DESCRIPTION = "Get notifications from realtime comics updates"
        const val CHANNEL_ID = "Comics Reader"
        const val NOTIFICATION_ID = 1
    }

    private val intent = Intent(context, LoginActivity::class.java).apply {
        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    }

    private val pendingIntent: PendingIntent =
        PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)

    private val builder = NotificationCompat.Builder(context, CHANNEL_ID)
        .setSmallIcon(R.drawable.rounded_corners)
        .setContentTitle(title)
        .setContentText(content)
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        .setContentIntent(pendingIntent)
        .setAutoCancel(true)

    private fun createNotificationChannel() {
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance).apply {
            this.description = CHANNEL_DESCRIPTION
        }

        val notificationManager: NotificationManager =
            getSystemService(context, NotificationManager::class.java)!!
        notificationManager.createNotificationChannel(channel)
    }

    fun sendNotification() {
        createNotificationChannel()
        with(NotificationManagerCompat.from(context)) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(
                    context,
                    "Permission denied. Cannot send notifications",
                    Toast.LENGTH_LONG
                ).show()
                return
            }
            notify(NOTIFICATION_ID, builder.build())
        }
    }

}