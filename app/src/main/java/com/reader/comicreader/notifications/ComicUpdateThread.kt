package com.reader.comicreader.notifications

import android.content.Context
import com.google.firebase.firestore.ListenerRegistration
import com.reader.comicreader.utils.Config
import com.reader.comicreader.utils.database.BookmarkDatabaseInterface
import java.time.Instant

class ComicUpdateThread(
    private val context: Context
) : Thread() {

    private var query: ListenerRegistration? = null

    private fun listen(timestamp: Long) {
        stopListen()
        query = BookmarkDatabaseInterface.subscribe(timestamp) { snapshot, e ->
            Config.log(this::class.java, "Running listener")
            if (e != null) {
                Config.log(this::class.java, "Service failed to listen")
                return@subscribe
            }
            if (snapshot != null && !snapshot.isEmpty) {
                Config.log(this::class.java, "Received an update!")
                val notification = ComicNotification(
                    context, "Comic updates!",
                    "One or more of your bookmarked comic has a new chapter (${snapshot.size()})!"
                )
                notification.sendNotification()
            }
        }
    }

    fun stopListen() {
        synchronized(this) {
            query?.remove()
        }
    }

    override fun run() {
        val timestamp = Instant.now().epochSecond

        Config.log(this::class.java, "Thread started, using timestamp: $timestamp")
        listen(timestamp)
    }
}