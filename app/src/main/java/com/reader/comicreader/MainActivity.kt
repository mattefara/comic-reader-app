package com.reader.comicreader

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.reader.comicreader.databinding.ActivityMainBinding
import com.reader.comicreader.notifications.NotificationService
import com.reader.comicreader.utils.Config
import com.reader.comicreader.utils.database.DatabaseInterface

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var authStateListener: AuthStateListener

    companion object {
        private const val JOB_ID = 100000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (DatabaseInterface.auth.currentUser == null) {
            goToLoginActivity()
        }

        authStateListener = AuthStateListener {
            Config.log(this::class.java, "Checking current user")
            if (it.currentUser == null) {
                goToLoginActivity()
            }
        }

        val navView = binding.navView

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_main) as NavHostFragment
        val navController = navHostFragment.navController

        navView.setupWithNavController(navController)
    }

    fun runService() {
        val job = JobInfo.Builder(JOB_ID, ComponentName(this, NotificationService::class.java))
            .setBackoffCriteria(100, JobInfo.BACKOFF_POLICY_LINEAR)
            .build()
        val scheduler = getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler
        val result = scheduler.schedule(job)
        if (result == JobScheduler.RESULT_SUCCESS) {
            Config.log(this::class.java, "Service scheduled")
        }
    }

    override fun onResume() {
        super.onResume()
        runService()
        DatabaseInterface.auth.addAuthStateListener(authStateListener)
    }

    override fun onPause() {
        super.onPause()
        DatabaseInterface.auth.removeAuthStateListener(authStateListener)
    }

    private fun goToLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    fun navigateTo(res: Int) {
        binding.navView.selectedItemId = res
    }

}