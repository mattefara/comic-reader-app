package com.reader.comicreader.ui.comicdetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.data.ComicCardDataClass
import com.reader.comicreader.data.ComicInterface
import com.reader.comicreader.data.document.ChapterDocument
import com.reader.comicreader.data.document.ComicDocument
import com.reader.comicreader.data.document.ProviderDocument
import com.reader.comicreader.utils.database.BookmarkDatabaseInterface
import com.reader.comicreader.utils.database.ComicDatabaseInterface
import kotlinx.coroutines.launch

class ComicDetailViewModel(application: Application) : AndroidViewModel(application) {

    private var currentComic: ComicInterface? = null

    val comicDocument = MutableLiveData<ComicDocument>()
    val comicId = MutableLiveData<String>()
    val bookmarkedComic = MutableLiveData<Boolean>()
    val lastChapter = MutableLiveData<ChapterDocument?>()
    val provider = MutableLiveData<ProviderDocument>()

    suspend fun getComic(comicId: String): ComicDocument {
        val comic = ComicDatabaseInterface.getOne(comicId)
        comicDocument.postValue(comic)
        return comic
    }

    fun comicResult(comicId: String) = liveData {
        emit(getComic(comicId))
    }

    fun updateComicInfo(comic: ComicInterface) {
        comicId.value = comic.comicId
        currentComic = comic
        when (comic) {
            is ComicCardDataClass -> {
                lastChapter.value = comic.firstChapter
                bookmarkedComic.value = false
            }
            is BookmarkCardDataClass -> {
                lastChapter.value = comic.lastChapterRead
                bookmarkedComic.value = true
            }
        }
    }

    fun updateBookmark(chapter: ChapterDocument) {
        if (bookmarkedComic.value == true) {
            val data = if (this.currentComic != null) {
                mapOf<String, Any>(
                    "updatedAt" to this.currentComic!!.updatedAt
                )
            } else {
                null
            }
            BookmarkDatabaseInterface.updateLastRead(comicId.value!!, chapter, data)
        }
        lastChapter.value = chapter
    }

    private fun addBookmark() {
        viewModelScope.launch {
            BookmarkDatabaseInterface.add(comicId.value!!, lastChapter.value!!)
        }
    }

    private fun removeBookmark() {
        viewModelScope.launch {
            BookmarkDatabaseInterface.remove(comicId.value!!)
        }
    }

    fun toggleBookmark() {
        if (comicId.value == null) {
            return
        }

        viewModelScope.launch {
            if (bookmarkedComic.value == false) {
                addBookmark()
            } else {
                removeBookmark()
            }
            bookmarkedComic.value = !bookmarkedComic.value!!
        }
    }

}