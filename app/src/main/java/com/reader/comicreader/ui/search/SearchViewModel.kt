package com.reader.comicreader.ui.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.data.ComicCardDataClass
import com.reader.comicreader.data.ComicInterface
import com.reader.comicreader.utils.database.BookmarkDatabaseInterface
import com.reader.comicreader.utils.database.ComicDatabaseInterface
import kotlinx.coroutines.launch

class SearchViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading = MutableLiveData(false)

    private fun loadingOperation(block: suspend () -> Unit) {
        isLoading.value = true
        viewModelScope.launch {
            block()
        }
        isLoading.value = false
    }

    val searchResults: MutableLiveData<List<ComicInterface>> by lazy {
        MutableLiveData(arrayListOf())
    }

    val searchText: MutableLiveData<String> by lazy {
        MutableLiveData("")
    }

    fun updateSearchText(searchText: String?) {
        this.searchText.value = searchText ?: ""
    }

    fun updateSearchResults() {
        loadingOperation {
            val bookmarks = BookmarkDatabaseInterface.getUpdates()
            val results =
                ComicDatabaseInterface.search(searchText.value!!, 50)
                    .map { comic: ComicCardDataClass ->
                        val bookmark =
                            bookmarks.find { bookmark -> bookmark.comicId == comic.comicId }
                        if (bookmark == null) {
                            return@map comic
                        } else {
                            return@map BookmarkCardDataClass(
                                comicId = bookmark.comicId,
                                title = bookmark.title,
                                cover = bookmark.attributes.cover,
                                updatesCount = bookmark.lastChapterPublished.chapterNumber
                                        - (bookmark.lastChapterRead?.chapterNumber ?: 0),
                                lastChapterRead = bookmark.lastChapterRead,
                                providerId = bookmark.providerId,
                                updatedAt = bookmark.updatedAt
                            )
                        }
                    }
            searchResults.value = results
        }
    }

}