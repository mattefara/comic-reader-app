package com.reader.comicreader.ui.bookmarks

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reader.comicreader.data.BookmarkAggregate
import com.reader.comicreader.utils.Config
import com.reader.comicreader.utils.database.BookmarkDatabaseInterface
import kotlinx.coroutines.launch

class BookmarkViewModel : ViewModel() {

    val isLoading by lazy { MutableLiveData(false) }
    val bookmarks by lazy { MutableLiveData<List<BookmarkAggregate>>(listOf()) }

    private fun loadingOperation(block: suspend () -> Unit) {
        isLoading.value = true
        viewModelScope.launch {
            block()
            isLoading.postValue(false)
        }
    }

    fun updateBookmarks() {
        loadingOperation {
            Config.log(this::class.java, "Getting bookmarks")
            bookmarks.postValue(
                BookmarkDatabaseInterface.aggregate(BookmarkDatabaseInterface.getUpdates())
            )
        }
    }

}