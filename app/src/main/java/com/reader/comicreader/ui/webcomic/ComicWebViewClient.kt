package com.reader.comicreader.ui.webcomic

import android.content.Context
import android.net.Uri
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast

class ComicWebViewClient(
    private val context: Context,
    private val providerRegex: String,
    private val onChangedPage: (url: Uri?) -> Unit
) : WebViewClient() {

    override fun shouldInterceptRequest(
        view: WebView?,
        request: WebResourceRequest?
    ): WebResourceResponse? {
        return super.shouldInterceptRequest(view, request)
    }

    override fun shouldOverrideUrlLoading(
        view: WebView?,
        request: WebResourceRequest?
    ): Boolean {
        val url = request?.url ?: return super.shouldOverrideUrlLoading(view, request)

        val shouldCancel = !providerRegex.toRegex().matches(url.path ?: "")
        if (shouldCancel) {
            Toast.makeText(
                context,
                "This view is only meant to read, cannot go elsewhere...",
                Toast.LENGTH_LONG
            ).show()
        }

        onChangedPage(url)
        return shouldCancel
    }
}