package com.reader.comicreader.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.SortedList
import com.reader.comicreader.R
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.data.ComicCardDataClass
import com.reader.comicreader.data.ComicInterface
import com.reader.comicreader.databinding.ItemUpdateCardBinding
import com.reader.comicreader.threading.ImageDownloader
import com.reader.comicreader.utils.list.FilterableRecyclerView
import com.reader.comicreader.utils.list.FilteredCallback

class FilteredSearchAdapter(
    private val onItemClickListener: (view: View, result: ComicInterface) -> Unit
) : FilterableRecyclerView<ComicInterface, FilteredSearchAdapter.FilteredSearchAdapterViewHolder>() {

    class FilteredSearchAdapterViewHolder(
        val view: View,
        private val onItemClickListener: (view: View, result: ComicInterface) -> Unit
    ) : BindableViewHolder<ComicInterface>(view) {
        private val binding: ItemUpdateCardBinding = ItemUpdateCardBinding.bind(view)

        override fun bind(item: ComicInterface) {
            val cover = when (item) {
                is ComicCardDataClass -> {
                    binding.updateCardCounter.visibility = View.GONE
                    item.cover
                }
                is BookmarkCardDataClass -> {
                    binding.updateCardCounter.visibility = View.VISIBLE
                    binding.updateCardCounter.text = item.updatesCount.toString()
                    item.cover
                }
            }

            view.setOnClickListener {
                onItemClickListener(it, item)
            }
            val downloader = ImageDownloader.Builder()
                .use(view.context)
                .into(binding.updateCardImageUrl)
                .load(cover)
                .build()
            downloader.download()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilteredSearchAdapterViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_update_card, parent, false)
        return FilteredSearchAdapterViewHolder(view, onItemClickListener)
    }

    override fun onCreateSortedList(): SortedList<ComicInterface> {
        return SortedList(
            ComicInterface::class.java,
            FilteredCallback(this) { o1, o2 ->
                o1.title.compareTo(o2.title)
            }
        )
    }

    override fun add(items: List<ComicInterface>) {
        this.items.addAll(items)
    }

    override fun remove(item: ComicInterface) {
        this.items.remove(item)
    }

    override fun remove(items: List<ComicInterface>) {
        items.forEach { item -> remove(item) }
    }

    override fun replaceAll(items: List<ComicInterface>) {
        this.items.beginBatchedUpdates()
        val toRemove = arrayListOf<ComicInterface>()
        for (i in 0 until this.items.size()) {
            val sortedItem = this.items.get(i)
            if (!items.contains(sortedItem)) {
                toRemove.add(sortedItem)
            }
        }
        remove(toRemove)
        add(items)
        this.items.endBatchedUpdates()
    }
}