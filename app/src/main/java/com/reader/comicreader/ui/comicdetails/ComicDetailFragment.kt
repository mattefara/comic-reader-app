package com.reader.comicreader.ui.comicdetails

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.reader.comicreader.R
import com.reader.comicreader.WebComicActivity
import com.reader.comicreader.data.document.ChapterDocument
import com.reader.comicreader.databinding.FragmentComicDetailBinding
import com.reader.comicreader.threading.ImageDownloader

class ComicDetailFragment : Fragment() {

    private var _binding: FragmentComicDetailBinding? = null
    private val model: ComicDetailViewModel by activityViewModels()

    private val binding get() = _binding!!

    private val getUpdatedBookmark =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode != Activity.RESULT_OK || model.bookmarkedComic.value != true) {
                return@registerForActivityResult
            }

            val chapterId = it.data?.extras?.getString("chapterId")
            val chapter =
                model.comicDocument.value?.chapters?.find { chapter -> chapter.chapterId == chapterId }

            if (chapter != null) {
                model.updateBookmark(chapter)
                updateButtons()
            }

        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentComicDetailBinding.inflate(inflater, container, false)

        toggleEmptyContent(true)

        model.comicId.observe(viewLifecycleOwner) { comicId ->
            toggleEmptyContent(false)
            model.comicResult(comicId).observe(viewLifecycleOwner) { comic ->
                downloadCover(comic.cover)

                binding.comicDetailTitle.text = comic.title
                binding.comicDetailChapterList.adapter =
                    ChapterListAdapter(comic.chapters) { _, item ->
                        openWebView(comic.chapters, item.chapterNumber)
                    }
            }
        }

        model.bookmarkedComic.observe(viewLifecycleOwner) {
            updateButtons()
        }

        model.lastChapter.observe(viewLifecycleOwner) {
            updateButtons()
        }

        binding.comicBookmarkButton.setOnClickListener {
            model.toggleBookmark()
        }

        binding.comicDetailReadContinue.setOnClickListener {
            model.lastChapter.value?.let { chapter ->
                openWebView(model.comicDocument.value?.chapters, chapter.chapterNumber)
            }
        }

        return binding.root
    }

    private fun updateButtons() {
        binding.comicBookmarkButton.text = if (model.bookmarkedComic.value == true) {
            resources.getString(R.string.remove_bookmark_button)
        } else {
            resources.getString(R.string.add_bookmark_button)
        }

        binding.comicDetailReadContinue.text = if (model.bookmarkedComic.value == true) {
            "${getString(R.string.continue_read_button)}:  ${model.lastChapter.value!!.chapterName}"
        } else {
            "${getString(R.string.start_read_button)}: ${model.lastChapter.value!!.chapterName}"
        }
    }

    private fun toggleEmptyContent(show: Boolean = true) {
        if (show) {
            binding.comicDetailContent.visibility = View.GONE
            binding.comicDetailEmptyContent.visibility = View.VISIBLE
        } else {
            binding.comicDetailContent.visibility = View.VISIBLE
            binding.comicDetailEmptyContent.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun downloadCover(cover: String) {
        val downloader = ImageDownloader.Builder()
            .use(requireContext())
            .into(binding.comicDetailImage)
            .load(cover)
            .build()
        downloader.download()
    }

    private fun openWebView(chapters: List<ChapterDocument>?, currentIndex: Int = 0) {
        if (chapters == null || chapters.size < currentIndex) {
            return
        }

        val chapter = chapters[chapters.size - currentIndex - 1]
        model.updateBookmark(chapter)

        val intent = Intent(requireContext(), WebComicActivity::class.java)

        intent.putExtra("comicId", model.comicId.value)
        intent.putExtra("providerId", model.comicDocument.value?.provider)
        intent.putExtra("lastRead", model.lastChapter.value?.chapterId)
        intent.putExtra("isBookmark", model.bookmarkedComic.value)

        getUpdatedBookmark.launch(intent)
    }
}