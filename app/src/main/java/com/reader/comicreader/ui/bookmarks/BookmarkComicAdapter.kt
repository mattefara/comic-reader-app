package com.reader.comicreader.ui.bookmarks

import android.view.View
import android.view.ViewGroup
import com.reader.comicreader.R
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.databinding.ItemUpdateCardBinding
import com.reader.comicreader.threading.ImageDownloader
import com.reader.comicreader.utils.list.BindableRecycleView

class BookmarkComicAdapter(
    comics: List<BookmarkCardDataClass>,
    private val onItemClickListener: OnItemClickListener<BookmarkCardDataClass>
) :
    BindableRecycleView<BookmarkCardDataClass, BookmarkComicAdapter.BookmarkComicAdapterViewHolder>(
        comics
    ) {

    class BookmarkComicAdapterViewHolder(
        private val view: View,
        private val onItemClickListener: OnItemClickListener<BookmarkCardDataClass>
    ) : BindableViewHolder<BookmarkCardDataClass>(view) {
        private val binding: ItemUpdateCardBinding = ItemUpdateCardBinding.bind(view)

        override fun bind(item: BookmarkCardDataClass) {
            binding.updateCardCounter.text = item.updatesCount.toString()
            view.setOnClickListener {
                onItemClickListener(it, item)
            }
            val downloader = ImageDownloader.Builder()
                .use(view.context)
                .into(binding.updateCardImageUrl)
                .load(item.cover)
                .build()
            downloader.download()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookmarkComicAdapterViewHolder {
        return BookmarkComicAdapterViewHolder(
            this.rootView(parent, R.layout.item_update_card),
            onItemClickListener
        )
    }
}