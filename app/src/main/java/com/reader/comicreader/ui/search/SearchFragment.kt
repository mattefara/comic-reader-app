package com.reader.comicreader.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.reader.comicreader.MainActivity
import com.reader.comicreader.R
import com.reader.comicreader.databinding.FragmentSearchBinding
import com.reader.comicreader.ui.comicdetails.ComicDetailViewModel

class SearchFragment: Fragment() {

    private var _binding: FragmentSearchBinding? = null

    private val binding get() = _binding!!
    private val model: SearchViewModel by viewModels()
    private val detailModel: ComicDetailViewModel by activityViewModels()

    private lateinit var adapter: FilteredSearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)

        adapter = FilteredSearchAdapter() { _, item ->
            detailModel.updateComicInfo(item)
            (requireActivity() as MainActivity).navigateTo(R.id.comicDetailFragment)
        }

        binding.searchResultList.addItemDecoration(SearchItemDecorator(3, 50))
        binding.searchResultList.adapter = adapter

        binding.searchRefresh.setOnRefreshListener {
            model.updateSearchResults()
        }

        model.isLoading.observe(viewLifecycleOwner) { loading: Boolean ->
            binding.searchRefresh.isRefreshing = loading
        }

        model.searchResults.observe(viewLifecycleOwner) {
            adapter.replaceAll(it)
        }

        model.searchText.observe(viewLifecycleOwner) {
            model.updateSearchResults()
        }

        binding.comicSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                model.updateSearchText(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                model.updateSearchText(newText)
                return true
            }

        })
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}