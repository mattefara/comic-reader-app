package com.reader.comicreader.ui.comicdetails

import android.view.View
import android.view.ViewGroup
import com.reader.comicreader.R
import com.reader.comicreader.data.document.ChapterDocument
import com.reader.comicreader.databinding.ItemComicDetailChapterBinding
import com.reader.comicreader.utils.list.BindableRecycleView

typealias OnItemClickListener<T> = (view: View, item: T) -> Unit

class ChapterListAdapter(
    chapters: List<ChapterDocument>,
    private val onItemClickListener: OnItemClickListener<ChapterDocument>
) : BindableRecycleView<ChapterDocument, ChapterListAdapter.ChapterListViewHolder>(chapters) {

    class ChapterListViewHolder(
        private val view: View,
        private val onItemClickListener: OnItemClickListener<ChapterDocument>
    ) : BindableViewHolder<ChapterDocument>(view) {
        val binding: ItemComicDetailChapterBinding = ItemComicDetailChapterBinding.bind(view)

        override fun bind(item: ChapterDocument) {
            binding.chapterDatailItem.text = item.chapterName
            view.setOnClickListener {
                onItemClickListener(it, item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChapterListViewHolder {
        return ChapterListViewHolder(
            rootView(parent, R.layout.item_comic_detail_chapter),
            onItemClickListener
        )
    }
}