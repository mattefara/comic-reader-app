package com.reader.comicreader.ui.bookmarks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.reader.comicreader.MainActivity
import com.reader.comicreader.R
import com.reader.comicreader.databinding.FragmentBookmarkBinding
import com.reader.comicreader.ui.comicdetails.ComicDetailViewModel
import com.reader.comicreader.utils.Config

class BookmarkFragment : Fragment() {

    private var _binding: FragmentBookmarkBinding? = null

    private val binding get() = _binding!!

    private val model: BookmarkViewModel by viewModels()
    private val detailModel: ComicDetailViewModel by activityViewModels()

    private lateinit var bookmarkAdapter: BookmarkAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBookmarkBinding.inflate(inflater, container, false)

        binding.bookmarkRefresh.setOnRefreshListener {
            model.updateBookmarks()
        }

        model.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.bookmarkRefresh.isRefreshing = isLoading
        }

        bookmarkAdapter = BookmarkAdapter() { _, comic ->
            detailModel.updateComicInfo(comic)
            (requireActivity() as MainActivity).navigateTo(R.id.comicDetailFragment)
        }
        binding.bookmarkList.adapter = bookmarkAdapter

        model.bookmarks.observe(viewLifecycleOwner) { bookmarks ->
            Config.log(this::class.java, "Updating the UI: $bookmarks")
            if (bookmarks.isEmpty()) {
                toggleEmptyContent(false)
                return@observe
            }
            toggleEmptyContent(true)
            bookmarkAdapter.replaceAll(bookmarks)

            binding.bookmarkRefresh.isRefreshing = false
        }


        model.updateBookmarks()

        return binding.root
    }

    private fun toggleEmptyContent(showList: Boolean) {
        if (showList) {
            binding.bookmarkList.visibility = View.VISIBLE
            binding.bookmarkEmpty.visibility = View.GONE
        } else {
            binding.bookmarkList.visibility = View.GONE
            binding.bookmarkEmpty.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}