package com.reader.comicreader.ui.bookmarks

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.SortedList
import com.reader.comicreader.R
import com.reader.comicreader.data.BookmarkAggregate
import com.reader.comicreader.data.BookmarkCardDataClass
import com.reader.comicreader.databinding.ItemBookmarkBinding
import com.reader.comicreader.threading.ImageDownloader
import com.reader.comicreader.utils.list.FilterableRecyclerView
import com.reader.comicreader.utils.list.FilteredCallback

typealias OnItemClickListener<T> = (view: View, item: T) -> Unit

class BookmarkAdapter(
    private val onItemClickListener: OnItemClickListener<BookmarkCardDataClass>
) :
    FilterableRecyclerView<BookmarkAggregate, BookmarkAdapter.BookmarkAdapterViewHolder>() {

    class BookmarkAdapterViewHolder(
        private val view: View,
        private val onItemClickListener: OnItemClickListener<BookmarkCardDataClass>
    ) : BindableViewHolder<BookmarkAggregate>(view) {
        private val binding: ItemBookmarkBinding = ItemBookmarkBinding.bind(view)

        override fun bind(item: BookmarkAggregate) {
            binding.itemBookmarkUpdateDate.text = item.formattedDate
            binding.itemBookmarkUpdates.adapter =
                BookmarkComicAdapter(item.updates, onItemClickListener)
            val downloader = ImageDownloader.Builder()
                .use(view.context)
                .into(binding.itemBookmarkProviderLogo)
                .load(item.providerLogo)
                .build()
            downloader.download()
        }
    }

    override fun onCreateSortedList(): SortedList<BookmarkAggregate> {
        return SortedList(
            BookmarkAggregate::class.java,
            FilteredCallback(this) { o1, o2 ->
                (o1.updatedAt - o2.updatedAt).toInt()
            }
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarkAdapterViewHolder {
        return BookmarkAdapterViewHolder(
            rootView(parent, R.layout.item_bookmark),
            onItemClickListener
        )
    }

    override fun add(items: List<BookmarkAggregate>) {
        this.items.addAll(items)
    }

    override fun remove(item: BookmarkAggregate) {
        this.items.remove(item)
    }

    override fun remove(items: List<BookmarkAggregate>) {
        items.forEach { item -> remove(item) }
    }

    override fun replaceAll(items: List<BookmarkAggregate>) {
        this.items.beginBatchedUpdates()
        val toRemove = arrayListOf<BookmarkAggregate>()
        for (i in 0 until this.items.size()) {
            val sortedItem = this.items.get(i)
            if (!items.contains(sortedItem)) {
                toRemove.add(sortedItem)
            }
        }
        remove(toRemove)
        add(items)
        this.items.endBatchedUpdates()
    }
}